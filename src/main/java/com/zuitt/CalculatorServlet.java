package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {
	
	public void init() {
		System.out.println("********************************");
		System.out.println("Initialized calculator servlet.");
		System.out.println("********************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println(
			"<h1>You are now using the calculator app</h1>" +
			"<p>To use the app, input two numbers and an operation.</p>" +
			"<p>Hit the submit button after filling in the details.</p>" +
			"<p>You will get the result shown in your browser!</p>"
		);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String operation = req.getParameter("operation");
		
		int result = 0;
		
		if (operation.trim().equalsIgnoreCase("add")) {
			result = num1 + num2;
		} else if (operation.trim().equalsIgnoreCase("subtract")) {
			result = num1 - num2;
		} else if (operation.trim().equalsIgnoreCase("multiply")) {
			result = num1 * num2;
		} else if (operation.trim().equalsIgnoreCase("divide")) {
			result = num1 - num2;
		}
		
		PrintWriter out = res.getWriter();
		
		out.println(
			"<p>The two numbers you provided are: " + num1 + ", " + num2 + "</p>" +
			"<p>The opertion that you wanted is: " + operation +
			"<p> The result is: " + result + "</p>"
		);
		
	}
	
	public void destroy() {
		System.out.println("********************************");
		System.out.println("Calculator servlet stopped.");
		System.out.println("********************************");
	}
	
}
